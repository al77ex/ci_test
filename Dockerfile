FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update >/dev/null && \
    apt-get install --no-install-recommends -y \
      php composer git vim \
      libapache2-mod-php7.2 \
        php7.2-curl \
        php7.2-json \
        php7.2-mbstring \
        php7.2-mysql \
        php7.2-xdebug \
        php7.2-xml \
        php7.2-zip \
    >/dev/null

COPY . /var/www/html

WORKDIR /var/www/html
RUN composer install

# Nope, that is not the best way, but for simplicity ¯\_(ツ)_/¯
CMD [ "php", "bin/console", "server:run", "*:8000" ]
